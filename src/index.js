const fs = require('fs');
const csv = require('csv-stream');
const through2 = require('through2')

const sleep = require('sleep');


function log(msg) {
  console.log(`[${new Date().toISOString()}] ${msg}`)
}

function newSleepPromise(seconds) {
  return new Promise(function(resolve, reject){
     setTimeout(() => resolve(seconds), seconds * 1000);
  })
}

function log2Memory(f) {
  const mem = () => `${Math.round((process.memoryUsage().heapUsed / 1024 / 1024) * 100) / 100} MB`
  const before = mem();
  f()
  const after = mem();
  log(`Memory used before: ${before}; after ${after}`)
}

function forceGC() {
   if (global.gc) {
      global.gc();
   } else {
      warn('No GC hook! Start your program as `node --expose-gc file.js`.');
   }
}

function gc() {
  log2Memory(forceGC)
}






function processDataStream(srcStream, callback, batchCondition, batchValue, endCallback, errorCallback, accumulator, initialValue) {
  const processNext = (currentPick, records, cb) => {
    const promise = callback({pick: currentPick, records: records})
    promise
      .then(() => cb())
      .catch((err) => cb(err))
  }

  if(undefined === accumulator) {
    accumulator = (acc, record) => {
      acc.push(record)
      return acc
    }
  }

  const initValue = (() => {
    if(undefined === initialValue) {
      return () => []
    } else if (typeof initialValue === 'function') {
      return initialValue
    } else {
      return () => initialValue
    }
  })()

  if (undefined === batchValue) {
    batchValue = (record) => record
  }

  if (undefined === endCallback) {
    endCallback = () => undefined
  }

  if (undefined === errorCallback) {
    errorCallback = (err) => console.error(err)
  }

  // All of these arguments are optional.
  const options = {
      delimiter : ',', // default is ,
      endLine : '\n', // default is \n,
      // columns : ['columnName1', 'columnName2'], // by default read the first line and use values found as columns
      // columnOffset : 2, // default is 0
      escapeChar : '"', // default is an empty string
      enclosedChar : '"' // default is an empty string
  }
  const csvStream = csv.createStream(options);

  srcStream
    .pipe(csvStream)
    .pipe((() => {
      let current // as currentClient
      let records = initValue()

      return through2(
        {objectMode: true},
        function(record, enc, cb) {
          // Initial case/value
          if(undefined === current) {
            current = batchValue(record)
          }

          if (batchCondition(current, record)) {
            processNext(current, records, cb)

            // Cleaning
            current = batchValue(record)
            records = accumulator(initValue(), record)
            gc()

          } else { // grouping elements
            records = accumulator(records, record)
            cb()
          }
        },
        (cb) => {
          processNext(current, records, cb)
          endCallback()
        }
      )
    })())
    .on('error', errorCallback)
}

//sleep.sleep(5)
//debugger
function getPick(record) {
  return record.pick
}

processDataStream(
  fs.createReadStream(__dirname + '/../data/100000_ordered.csv'),
  (batch) => {
    log(`New batch through processDataStream for pick ${batch.pick} with length ${batch.records.length}`)
    const saveDbPromise = newSleepPromise(1)

    return saveDbPromise // FIXME: Will be replaced by Promise.all(dbQueries)
  },
  (current, record) => current !== getPick(record),
  (record) => getPick(record),
  () => log("Ended!!")
)
